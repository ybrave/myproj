
import FirstLayer


class AppLayer:
	def __init__(self, ipAdress, port):
			self.s = FirstLayer.FirstLayer(ipAdress,port)
					
	def move(self,x, y, z):
		return self.s.command("move",x,y,z)
		
	def moveby(self,x, y, z):
		return self.s.command("moveby", x, y, z)
		
	def where(self):
		return self.s.command("where")
		

# x = AppLayer('127.0.0.1', 5005)
# res1 = x.where()
# if res1 == 'where':
	# print("helloworld")
# else:
	# print("good day sir")