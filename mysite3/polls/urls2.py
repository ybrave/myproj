from django.conf.urls import patterns, url

from polls import views

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
	url(r'^MyBlockly', views.index, name='index'),
)