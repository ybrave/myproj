#!C:\Python34\python.exe

import cgi
import cgitb; cgitb.enable()  # for troubleshooting

print "Content-type: text/plain"
print

form = cgi.FieldStorage()
x = int(form.getvalue("x", "0"))
y = int(form.getvalue("y", "0"))
z = int(form.getvalue("z", "0"))

print "x=%d, y=%d, z=%d" % (x, y, z)

