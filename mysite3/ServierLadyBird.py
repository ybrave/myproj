#!/usr/bin/env python
 
import socket
 
 
TCP_IP = '127.0.0.1'
TCP_PORT = 5005
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response
 
pos = (0, 0, 0)
def move(desired, conn):
	global pos
	pos = desired
	conn.send('\n')

def where(conn):
	conn.send(' '.join([str(x) for x in pos]) + '\n')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

while 1:
	conn, addr = s.accept()
	print('Connection address:', addr)
	while 1:
		data = conn.recv(BUFFER_SIZE)
		if not data: break
		print("received data:", data)
		parsed = data.split(' ')
		if parsed[0] == 'move':
			move(tuple((int(x) for x in parsed[1:])), conn)
		elif parsed[0] == 'where':
			where(conn)
	conn.close()
	
