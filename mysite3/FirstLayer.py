
import socket

class FirstLayer:
	def __init__(self, ipAdress, port):
			self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.s.connect((ipAdress, port))
			
	def command(self, cmd, *args):
		message = cmd 
		for	arg in args:
			message += " " + str(arg)
		self.s.send(bytes(message + "\n", 'UTF-8'))
		data = self.s.recv(1024).decode('UTF-8')
		
		if data[-1] != "\n":
			raise Exception("illegal response: " + data)
		#print("received data:", data)
		response = data[:-1].split(' ')
		if response[0] == 'ERROR':
			raise Exception(' '.join(response[1:]))
		return ' '.join(response)
		
	def __del__(self):
		self.s.close()
		

# x = FirstLayer('127.0.0.1', 5005)
# res1 = x.command("move_2",3, 4, 5)
# if res1 != 'move_2 3 4 5':
	# raise Exception(res1)

# try:
	# res2 = x.command('ERROR', 'This is an error')
	# print('FAILURE: Should throw')
# except Exception as e:
	# if str(e) != 'This is an error':
		# raise Exception('Bad exception: ' + str(e))
