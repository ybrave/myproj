from django.conf.urls import patterns, include, url
from django.contrib import admin

from polls import views


urlpatterns = patterns('',
    url(r'^polls/', include('polls.urls')),
    url(r'^admin/', include(admin.site.urls)),
	url(r'^start/', include('polls.urls2')),
	url(r'^MyBlockly', views.index, name='index'),
)
